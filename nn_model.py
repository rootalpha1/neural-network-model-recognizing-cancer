from tensorflow import keras
from tensorflow.keras import layers
import os
import numpy as np
import matplotlib.pyplot as plt

#the train data path
Dataset_Train_Dir = "/Added-Dataset1&2&3_bg/train"
#the validation data path
Dataset_Validation_Dir = "/Added-Dataset1&2&3_bg/validation"

#using keras generator 
from tensorflow.keras.utils import image_dataset_from_directory
train_dataset = image_dataset_from_directory(Dataset_Train_Dir,labels='inferred',image_size=(224,224),batch_size=32)
validation_dataset = image_dataset_from_directory(Dataset_Validation_Dir,labels='inferred',image_size=(224,224),batch_size=32)

#here using data_augmentation because our data not too much and need more data for better accuracy
data_augmentation = keras.Sequential(
  [
    layers.RandomFlip("horizontal"),
    layers.RandomRotation(0.1),
    layers.RandomZoom(0.2),
    #layers.RandomRotation(0.5),
    #layers.Rescaling(1./255),
  ]
)

data_augmentation2 = keras.Sequential(
    [
      layers.RandomFlip("horizontal"),
      layers.RandomRotation(0.5),
      layers.Rescaling(1./255),
    ]
)


'''plt.figure(figsize=(10, 10))
for images, _ in train_dataset.take(1):
  for i in range(9):
    augmented_images = data_augmentation(images)
    ax=plt.subplot(3,3,i+1)
    plt.imshow(augmented_images[0].numpy().astype("uint8"))
    plt.axis("off")'''

#create our model
inputs = keras.Input(shape=(224, 224, 3))
x = data_augmentation(inputs)
x = data_augmentation2(x)
x = layers.Conv2D(filters=32, kernel_size=3, activation="relu")(x)
x = layers.MaxPooling2D(pool_size=2)(x)
x = layers.Conv2D(filters=64, kernel_size=3, activation="relu")(x)
x = layers.MaxPooling2D(pool_size=2)(x)
x = layers.Conv2D(filters=128, kernel_size=3, activation="relu")(x)
x = layers.MaxPooling2D(pool_size=2)(x)
x = layers.Conv2D(filters=256, kernel_size=3, activation="relu")(x)
x = layers.MaxPooling2D(pool_size=2)(x)
x = layers.Conv2D(filters=256, kernel_size=3, activation="relu")(x)
x = layers.Flatten()(x)
x = layers.Dropout(0.5)(x)
outputs = layers.Dense(3, activation="softmax")(x)
model = keras.Model(inputs=inputs, outputs=outputs)

model.compile(loss="sparse_categorical_crossentropy",
optimizer="rmsprop",
metrics=["accuracy"])

callbacks = [
keras.callbacks.ModelCheckpoint(
filepath="convnet_from_scratch_with_augmentation.keras",
save_best_only=True,
monitor="val_loss")
]


history = model.fit(
train_dataset,
epochs=60,
validation_data=validation_dataset,
callbacks=callbacks)
